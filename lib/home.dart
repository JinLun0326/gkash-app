import 'package:flutter/material.dart';
import 'package:gkash/webview_screen/PaymentRequest.dart';
import 'package:gkash/webview_screen/payment_webview.dart';
import 'common.dart';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';


class PaymentPage extends StatefulWidget {
  PaymentPage({Key key}) : super(key: key);
  @override
  _MyHome createState() => new _MyHome();
}

class _MyHome extends State<PaymentPage> {
  TextEditingController _amountController = new TextEditingController();
  GlobalKey _fKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurpleAccent[700],
        title: Text("Gkash App Intergration"),
      ),
      backgroundColor: white,
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 24.0),
        child: Form(
          key: _fKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TextFormField(
                  controller: _amountController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: "Amount", 
                      hintText: "Please enter payment",
                      ),
                  validator: (v) {
                    return v.trim().length > 0 ? null : "Amount cannot be null";
                  }),
              Padding(
                padding: const EdgeInsets.only(top: 28.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        child: Text('SUBMIT'),
                        padding: EdgeInsets.all(10),
                        textColor: Colors.white,
                        color: Theme.of(context).primaryColor,
                        onPressed: () async {
                          if ((_fKey.currentState as FormState).validate()) {
                            String version = "1.5.1";
                            String cid = 'M161-U-33';
                            String currency = 'MYR';
                            String signatureKey = 'oAhVwtUxfrop4cI';
                            String cartID = new DateFormat('yyyyMMddHHmmss')
                                .format(DateTime.now());
                            String amount = double.parse(_amountController.text)
                                .toStringAsFixed(2);

                            final paymentRequest = PaymentRequest(version, cid,
                                currency, amount, cartID, signatureKey,
                                callbackUrl:
                                    "https://paymentdemo.gkash.my/callback.php",
                                email: "abc123@gmail.com",
                                mobileNo: "012-4446633");

                            var urlLink = await Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return PaymentWebView(
                                  paymentRequest: paymentRequest);
                            }));

                            var urlData = "";

                            Uri url = Uri.parse(urlLink);

                            urlData = "Status:" +
                                url.queryParameters['status'] +
                                "\nDescription: " +
                                url.queryParameters['description'] +
                                "\nCID: " +
                                url.queryParameters['CID'] +
                                "\nPOID: " +
                                url.queryParameters['POID'] +
                                "\nCartId: " +
                                url.queryParameters['CartId'] +
                                "\nAmount: " +
                                url.queryParameters['Amount'] +
                                "\nCurrency: " +
                                url.queryParameters['Currency'] +
                                "\nPaymentType: " +
                                url.queryParameters['PaymentType'];

                            print(
                                '\n------------------------------------------------------------');
                            print(urlLink);
                            print(urlData);
                            print(
                                '\n------------------------------------------------------------');

                            Fluttertoast.showToast(
                                msg: urlData,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 7,
                                toastLength: Toast.LENGTH_LONG);
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
