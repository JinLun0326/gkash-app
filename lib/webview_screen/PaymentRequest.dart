import 'dart:convert';
import 'package:crypto/crypto.dart';

class PaymentRequest{
  final String version;
  final String cid;
  final String currency;
  final String amount;
  final String cartId;
  final String signature;
  final String callbackUrl;
  final String returnUrl;
  final String email;
  final String mobileNo;
  final String firstName;
  final String lastName;
  final String productDescription;
  final String billingStreet;
  final String billingPostCode;
  final String billingCity;
  final String billingState;
  final String billingCountry;

  PaymentRequest(
    this.version,
    this.cid,
    this.currency,
    this.amount,
    this.cartId,
    this.signature,
    {
      this.callbackUrl,
      this.returnUrl,
      this.email,
      this.mobileNo,
      this.firstName,
      this.lastName,
      this.productDescription,
      this.billingStreet,
      this.billingPostCode,
      this.billingCountry,
      this.billingCity,
      this.billingState,
    }
  );


  String generateSignature(){
    String sign = (signature + ";" +
     cid + ";" + cartId + ";" + amount.replaceAll(".", "") + ";" + currency).toUpperCase();
    var digeset = sha512.convert(utf8.encode(sign));

    return digeset.toString().toLowerCase();
  }
}